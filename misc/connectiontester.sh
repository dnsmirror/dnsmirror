#!/bin/bash

# to do: allow & detect an IP as parameter

if (( $# == 0 )); then
        echo "usage: $0 \$FQDN [\$PORT] (default:443) [4|6] (default: 4, for IPv4 testing) [SLEEPDURATION] (default: 60)"
        exit 1
fi
FQDN=$1

# defaults:
PORT=443
IPPROTO=4
SLEEPDURATION=60

if (( $# >= 2 )); then
        PORT=$2
fi
if (( $# >= 3 )); then
        IPPROTO=$3
fi
if (( $# >= 4 )); then
        SLEEPDURATION=$4
fi


if [[ $IPPROTO -eq 4 ]]; then
        RRTYPE=A
elif [[ $IPPROTO -eq 6 ]]; then
        RRTYPE=AAAA
else
        echo "unknown IPPROTO $IPPROTO"
        exit 2
fi

cd /tmp

while true; do
        IP=$(dig +short $RRTYPE $FQDN | head -n1)
        netcat -z -w 1 $IP $PORT
        if [ $? -eq 0 ] ; then
                echo ok $FQDN $PORT $(date) $IP
        else
                echo bad $FQDN $PORT $(date) $IP
        fi

        sleep $SLEEPDURATION
done
