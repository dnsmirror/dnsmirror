#!/bin/bash

# SPDX-License-Identifier: GPL-3.0-or-later
#
#    This file is part of dnsmirror.
#
#    dnsmirror is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    dnsmirror is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Carsten Weber 2024

if (( $# == 0 )); then
        echo "usage: $0 \$ZONE [\$COUNT] [\$STARTIP] [\$RRTYPE] [\$SUBDOMAIN] [\$HOSTNAME]"
	echo ""
	echo "to fill your dynamic DNS zone with test data"
        echo "adding \$COUNT IPs to \$HOSTNAME.\$SUBDOMAIN.\$ZONE - e. g. invalid.test.dnsmirror.example.com"
        echo "\$ZONE - the parent zone, to which the records are added"
	echo "[\$COUNT] (default: 3) - how many IPs to add"
	echo "[\$STARTIP] (default: 10.10.0.1) - the first IP to add"
	echo "[\$RRTYPE] (fixed: A) - only A implemented, cannot yet increment for IPv6"
	echo "[\$SUBDOMAIN] (default: test)"
	echo "[\$HOSTNAME] (default: invalid)"
        exit 1
fi
ZONE=$1

# defaults:
COUNT="3"
STARTIP="10.10.0.1"
RRTYPE="A"
SUBDOMAIN="test"
HOSTNAME="invalid"

if (( $# >= 2 )); then
	COUNT=$2
fi
if (( $# >= 3 )); then
	STARTIP=$3
fi
if (( $# >= 4 )); then
	#RRTYPE=$4
	:
fi
if (( $# >= 5 )); then
	SUBDOMAIN=$5
fi
if (( $# >= 6 )); then
	HOSTNAME=$6
fi


a=$(echo "$STARTIP"|cut -d"." -f1)
b=$(echo "$STARTIP"|cut -d"." -f2)
c=$(echo "$STARTIP"|cut -d"." -f3)
d=$(echo "$STARTIP"|cut -d"." -f4)
STARTIP=$((a*16777216+b*65536+c*256+d)) # convert IP from dotted decimal to decimal format

FQDN="${HOSTNAME}.${SUBDOMAIN}.${ZONE}"


echo "adding $COUNT RRs to $FQDN"

I=0
while [ $I -lt ${COUNT} ]; do
	IP=$((STARTIP+I))

	a=$((IP/16777216)); IP=$((IP%16777216))
	b=$((IP/65536)); IP=$((IP%65536))
	c=$((IP/256)); IP=$((IP%256))
	IP=$a.$b.$c.$IP # convert IP from decimal to dotted decimal format
	#echo $IP
	# bug: this happily calculates IPs beyond 255.255.255.255

	echo -e "zone ${ZONE}\nupdate add ${FQDN} 60 ${RRTYPE} ${IP}\nsend\n"|nsupdate -g
	echo -en "$((I+1))/$COUNT $IP        \r"


	I=$((I+1))
done
echo ""
