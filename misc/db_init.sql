CREATE DATABASE dnsmirror;

CREATE TABLE dnsmirror.dnscache (
	fqdn varchar(1004),
	ip varchar(61),
	rrtype varchar(16),
	lastseen int,
	UNIQUE INDEX(fqdn,ip,rrtype),
	INDEX(lastseen) )
        CHARACTER SET 'ascii',
        COLLATE 'ascii_bin' ;

CREATE USER dnsmirror@localhost IDENTIFIED BY "changeme436564545";

GRANT SELECT,INSERT,UPDATE,DELETE ON dnsmirror.dnscache TO dnsmirror@localhost;

GRANT LOCK TABLES ON dnsmirror.* TO dnsmirror@localhost;

CREATE TABLE dnsmirror.fqdns (
	fqdn varchar(1004),
	rrtype varchar(16),
	UNIQUE INDEX(fqdn,rrtype) )
	CHARACTER SET 'ascii',
	COLLATE 'ascii_bin' ;
