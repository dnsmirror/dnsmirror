#!./venv/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

# near-minimum version of dnsmirror to show the general principle of operation

# requires: dnspython
import dns.resolver
import subprocess

fqdns = ["example.com", "example.org" ]
dnsservers = ["192.168.1.1", "192.168.1.2", ]
rrtypes = ["A", "AAAA"]
dnsmirrorzone = "dnsmirror.example.com"
ttl = 60

for dnsserver in dnsservers:
	for fqdn in fqdns:
		for rrtype in rrtypes:
			resolver = dns.resolver.Resolver(configure=False)
			resolver.nameservers = [dnsserver]
			try:
				response = resolver.resolve(fqdn, rrtype)
			except Exception as e:
				print(dnsserver+" "+repr(e))
				break
			for ip in response:
				print(f"pushing answer from {dnsserver} for FQDN {fqdn}: {ip} {rrtype}")
				subprocess.run(["nsupdate", "-g"], input=f"zone {dnsmirrorzone}\nupdate add {fqdn}.demo.{dnsmirrorzone} {ttl} {rrtype} {ip}\nsend\n".encode(), check=True)
