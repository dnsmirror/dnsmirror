#!/bin/sh

# SPDX-License-Identifier: GPL-3.0-or-later
#
#    This file is part of dnsmirror.
#
#    dnsmirror is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    dnsmirror is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Carsten Weber 2024


# Query an authoritative DNS server (which potentially knows more ressource
# records than it sends at once) repeatedly, until it does not return new IPs
# few times in a row.
#
# This is helpful to measure the amount of RRs for a given FQDN, and intented
# to work with a DNS server like Windows Server, which returns just 1000 RRs
# at a time and then shifts the list, so you need to query $COUNT-1000 times.
#
# When you don't specify a DNS server and your queries end on caching proxies
# instead of an authoritative server, the result probably will be that you
# never will see more IPs than the authoritative DNS server returns at one
# time. When your queries hit multiple servers (caching or authoritative),
# the result is undefined.
#
# ISSUES:
# When the authoritative DNS server changes the list of RRs, it might reset
# it's round-robin pointer to the start of the list. Then this script might
# detect 100% already-known responses several times in a row, which is used
# as sign that the complete list of RRs has been gathered. This could be
# improved by detecting sudden jumps in the list, e. g. by comparing one
# IP result set (dig response) to the result set from the iteration before,
# and when suddenly the diff is very large, it is assumed that such reset
# of the pointer happened.

# mkpipe
# dig +tct +keepopen +ignore +f PIPE
# stream in requests, stream out responses


cleanup () {
	if [ "$DEBUG" -ne "1" ]; then
		rm -f "$RRFILE"
		rm -f "$RRFILE.dig"
		rm -f "$RRFILE.new"
		cd /
		rmdir "$TEMPDIR"
	fi
}

STARTTIME=$(date +%s)
DEBUG=0

if [ "$#" -eq 1 ]; then
	FQDN="$1"
elif [ "$#" -eq 2 ]; then
	FQDN="$1"
	DNSSERVER="@$2"
elif [ "$#" -eq 3 ]; then
	FQDN="$1"
	DNSSERVER="@$2"
	DEBUG=1
else
	echo "usage: $0 FQDN [DNSSERVER]"
	exit 1
fi

TEMPDIR=$(mktemp --tmpdir -d count_RRs_XXXXXXXX)
RRFILE=rrfile.$(date "+%Y%m%d-%H%M%S")

trap cleanup 0 # EXIT

cd "$TEMPDIR" || exit 2
touch "$RRFILE"
COUNT=0           # how many unique IPs were returned
SAMECOUNT=0       # how often were no new IPs returned from the server
MAXSAMECOUNT=4    # how often to retry to get unknown IPs from the server
STAT_DNSQUERIES=0 # how many queries were sent to get the result
while true; do
	LASTCOUNT="$COUNT"

	# $DNSSERVER variable below must not be quoted, else dig sees an empty parameter in case no DNS server was provided
	dig $DNSSERVER +short "$FQDN" > "$RRFILE.dig"
	cat "$RRFILE" "$RRFILE.dig" | sort | uniq > "$RRFILE.new"
	mv "$RRFILE.new" "$RRFILE"

	COUNT=$(wc -l "$RRFILE"|cut -d' ' -f1)
	STAT_DNSQUERIES=$((STAT_DNSQUERIES+1))
	if [ "$COUNT" -eq "$LASTCOUNT" ]; then
		SAMECOUNT=$((SAMECOUNT+1))
	fi
	if [ "$SAMECOUNT" -ge "$MAXSAMECOUNT" ] ; then
		ENDTIME=$(date +%s)
		RUNTIME=$((ENDTIME-STARTTIME))
		if [ "$DEBUG" -eq "1" ]; then
			DEBUGSTRING=$TEMPDIR
		fi
		echo "$(date '+%Y-%m-%d %H:%M:%S %Z') $FQDN $COUNT $STAT_DNSQUERIES $RUNTIME $DEBUGSTRING"
		exit
	fi
done

echo "UNEXPECTED STATE" # should never get reached
