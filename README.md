# dnsmirror

*Carsten Weber (carwe.devel at carwe dot de)*


<!-- TOC start (generated with https://github.com/derlin/bitdowntoc) -->

- [0 quickstart](#0-quickstart)
- [1 short description](#1-short-description)
   * [1.1 works even when](#11-works-even-when)
   * [1.2 works not (good) when](#12-works-not-good-when)
- [2 reasoning](#2-reasoning)
- [3 proposal](#3-proposal)
   * [3.1 assumptions](#31-assumptions)
   * [3.2 speed](#32-speed)
   * [3.3 security considerations](#33-security-considerations)
   * [3.4 DNS backends](#34-dns-backends)
- [4 implementation](#4-implementation)
   * [4.1 dnsmirror-collector](#41-dnsmirror-collector)
   * [4.2 dnsmirror-zoneserver](#42-dnsmirror-zoneserver)
   * [4.3 data transfer from collector to server](#43-data-transfer-from-collector-to-server)
   * [4.4 stages](#44-stages)
      + [4.4.1 collector](#441-collector)
      + [4.4.2 cache](#442-cache)
      + [4.4.3 backend](#443-backend)
      + [4.4.4 tags for stage implementations](#444-tags-for-stage-implementations)
- [5 open to-do](#5-open-to-do)
- [6 init](#6-init)
   * [6.1 DBMS init](#61-dbms-init)
   * [6.2 database init](#62-database-init)
   * [6.3 python init](#63-python-init)
   * [6.4 config init](#64-config-init)
   * [6.5 windows DNS zone init](#65-windows-dns-zone-init)
   * [6.6 OS init](#66-os-init)
      + [6.6.1 ulimit](#661-ulimit)
      + [6.6.2 Kerberos](#662-kerberos)

<!-- TOC end -->

<!-- TOC --><a name="0-quickstart"></a>
## 0 quickstart

on Debian:

    apt-get install dnsutils build-essential python3-venv libmariadb-dev python3-dev git
    (maybe) apt-get install krb5-user
    git clone https://codeberg.org/dnsmirror/dnsmirror.git /opt/dnsmirror
    cd /opt/dnsmirror
    make
    cd source
    cp .config.template .config
    chmod g=,o= .config
    vim .config
    (maybe) kinit
    ./start --debug=INFO --loop=300



<!-- TOC --><a name="1-short-description"></a>
## 1 short description

Goal is to allow a client access via a firewall to a domain name (FQDN) which has complex/shortlived DNS records (IPs) without modifying the client or firewall (besides replacing the FQDN in the allow rule), or tampering with traffic, by updating a dynamically created dns zone so a firewall can learn "all" IPs for a given FQDN.


<!-- TOC --><a name="11-works-even-when"></a>
### 1.1 works even when

* the client is using any version of HTTP/HTTPS/QUIC with any version of SSL/TLS, including TLS 1.3 with ECH (former ESNI), or any other protocol
* the client is out of your control, and you cannot edit proxy settings or the certificate store or the client is doing cert/CA pinning
* the client is using a hardcoded DNS server, or DoT/DoH to obtain IPs to connect to
* the client is deeply layered in some automatically deployed/updated container setup where you do not want to edit proxy settings or add a root certificate (CA) to the certificate store
* the client is verifying DNS responses via DNSSEC
* the authoritative DNS servers return just few IPs out of a large pool, with just few seconds livetime (TTL)


<!-- TOC --><a name="12-works-not-good-when"></a>
### 1.2 works not (good) when

* the authoritative DNS servers serve randomly from a HUGE (several thousands) pool of IP addresses (I'm looking at you, Docker), as this may overwhelm the used internal DNS server or the firewall (splitting the IPs over multiple dynamically created names probably helps)
* the authoritative DNS servers are replacing the IPs in the pool very fast
* the client uses a DNS hardcoded server which is not officially authoritative/has different views depending on where the request comes from


<!-- TOC --><a name="2-reasoning"></a>
## 2 reasoning

Firewalls can use FQDNs in ACLs, but the actual IP packages do not contain the FQDN anymore. It works (besides some other imperfect ways), as the firewall regularily resolves the FQDN and then allows the returned IP addresses as destinations.

When now the client asks a different DNS server/cache than the firewall, or the responses from upstream DNS servers vary over time or per request, especially with a short TTL, it may happen that the client tries to contact an IP address that the firewall does not know about at that moment, as it is hidden to it.

When the firewall gets to know about all the IP addresses the client COULD try to contact, functioning connection is guaranteed. Some firewall products not support features like TLS inspection and even if, they maybe never CAN support this 100% reliably given that the client and the firewall may use different upstream DNS servers and the firewall may not in all cases be able to intercept that (DoT, DoH, ...?) or infer the destination from the content of the connection (TLS 1.3 ECH), especially as there are also/still connections which are not HTTP/HTTPS.

Goal is to allow as many related (to a given FQDN) IPs as possible, but it is not possible to prevent access to IPs which 
1. are not related to a given FQDN anymore - IPs stay cached until the timeout expires, there is no way to IMMEDIATELY know when a given FQDN is not served by a given IP anymore, except by actually testing the connection (if HTTP/S), or otherwise prove that the authoritative nameservers would NOT answer that IP to somebody. The timeout removes an IP when it was not observed as DNS answer for a configurable amount of time.
2. additionally to a given FQDN allow access to MORE FQDNs than desired. Webservers host many FQDNs, there is no 1:1/1:n relation between FQDNs and IPs anymore, but m:n.

Generally the path followed here is connectivity-over-security.


<!-- TOC --><a name="3-proposal"></a>
## 3 proposal

To allow the firewall to learn as many related IP addresses as possible, we can install a helper service which builds a list of all these IPs and exports them via DNS, so the firewall can stay the same except a slightly changed FQDN in some ACEs. A replacement of the upstream DNS server used by the firewall is possible, this server could inject the other/hidden DNS RRs, but this seems a too large risk for firewall operation. Better is to only query the dnsmirror helper service for just a few FQDNs. This requires these FQDNs to live in a domain which is otherwise unused and which can be delegated or otherwise made available for write operations to the dnsmirror helper.

This can be done by shadowing an existing domain, but in any case this (sub-)domain should not be used for anything else. This can also be an unused TLD, but there are new TLDs in use every month, so this is a gamble.

For example we could attach .dnsmirror or .dnsmirror.example.com to all FQDNs we wish to be forwarded to the dnsmirror helper and insert a forward entry into our regular DNS infrastructure, or just delegate the domain officially to the dnsmirror helper.


<!-- TOC --><a name="31-assumptions"></a>
### 3.1 assumptions

The firewall is able to understand more than one IP per FQDN, and is able to understand "enough" IPs. (real-world FQDNs have easily over 100 actively used IPv4+IPv6 IPs behind them)
* tested with 33 IPs on Cisco ASA - need to retest with more IPs

The queried nameservers in sum/over time approximate knowledge of all IPs any given FQDN resolves to. The coverage can maybe get improved/high coverage reached more quickly, when the dns collector queries authoritative nameservers directly. For the beginning however, the implementation will ask a fixed set of recursing nameservers only. An extension then would be to let it recurse itself and ask the authoritative nameservers directly, avoiding the TTL of caching proxies.

It is assumed that the client asks DNS servers which share the same general VIEW as the collector service. You will get insufficient/nonworking results if the client asks a(n internal) hardcoded nameserver of an organisation directly which is not seen as authoritative by the rest of the world, or, which responds a different set of IPs (from a different DNS VIEW) to the client (for example IPs from RFC1918) than to the DNS servers which the collector queries.

The DNS zone + subdomain which is used to host the mirror-FQDNs should be rather short. It MUST be short enough so that the longest FQDN you want to mirror plus the subdomain plus the zone itself plus the label separators (dots) fit into 255 bytes (remember the trailing dot). The longest possible FQDN you can mirror is 255-len(".SUBDOMAIN.DNSMIRROR.EXAMPLE.COM.")


<!-- TOC --><a name="32-speed"></a>
### 3.2 speed

To allow ALL valid connections (without just allowing access to any IP of course) it is necessary that the firewall learns about new IPs BEFORE the first client tries to connect to it. This goal can only be approximated, some IPs (especially when newly used to serve that FQDN) may be unreachable by some or all clients for a varying amount of time. The timeout value influences these events, as a shorter timeout value makes IPs disappear faster from the mirror zone, despite it maybe is still in use to serve this FQDN, just not visible to any asked DNS server at the moment he was asked.

A client may ask that very DNS server earlier or later and get an IP address as answer, which the collector never maybe sees: Short TTL of answers may lead to a situation where a client learns an IP, but the collector, even when asking the same DNS server, does not learn it as the TTL already expired and the DNS server answered a different IP to the collector now. Asking DNS servers more often decreases the likelyhood of this event, but increases overall load. Even asking authoritative nameservers may not solve this issue fully.


<!-- TOC --><a name="33-security-considerations"></a>
### 3.3 security considerations

* attack on collector, database (SQL injection), backend (AD)  etc. by crafting special DNS responses
  * especially when it got implemented that the collector asks authoritative nameservers instead of only a fixed list of DNS servers which probably (hopefully) are filtering invalid DNS responses instead of passing them on
* injecting IPs as allowed destinations into the firewall rules by controlling the content (IP addresses) of a zone
  * An attacker may own a domain, which contains a service/information which allows the attacker to argument that a firewall is needed to allow access to it. He crafts the zone content so that the IPs are fastchanging, the answer size is small (just a small subset of all IPs) and the TTL is short - so that usage of this dnsmirror implementation seems necessary to reliably allow access to it. As soon as this access was granted, the attacker could add any IP he wants access to into the zone he controls, and the firewall will learn it as allowed destination shortly after.
  * This attack is not unique to dnsmirror, but it can be done more targeted/less obvious, by answering the additional IPs only to one of the DNS servers which are queried by the collector. So if the attacker is in Europe and the firewall is in Europe too, but the collector queries DNS servers on other continents too, it may be possible to reply the additional IPs only to the DNS servers on one other continent, hiding them from an IT department sitting in Europe, which probably would not use a DNS server from a different continent while checking the contents of an FQDN/the list of IPs used.


<!-- TOC --><a name="34-dns-backends"></a>
### 3.4 DNS backends

The place where the DNS records are made available for the firewall to collect could be any DNS server, like BIND, Windows-DNS, or any other mechanism for the transport from the collector service towards firewall (dynamic [via DNS] or static [via API, CLI etc.]) configuration.

To separate from that issue is the question, if an intermediate storage (like a database) exists, which is fed from the collector service, maybe regularily cleaned from entries which reached a timeout, and from where the actual backend is then provisioned.

* use an API to add group objects/alias lists to the firewall static configuration - out of scope
* use a primary DNS zone of a BIND service, filled by some mechanism (TSIG, or generate fresh zone files and push them under the feets of BIND), and maybe set up secondaries to spread the load
* use a Windows Server, maybe DCs, to host a zone there (maybe AD-integrated -> automatically syncing)
  * Note that this requires the GSS-TSIG update mechanism and creates dynamic records, which could get scavanged by a server-integrated aging mechanism. While it is not possible to create non-aging (static) RRs via DDNS, scavanging could be disabled for that zone, a long lifetime (up to 2^31-1) could be chosen, or (maybe depending on implementation, at least true for Windows-DNS) one RR could be set to static and all RRs in the same RRset coming after that would inherit that property.
  * Note that, while Windows DCs DO sync the zone between them, this can take longer than needed to reliably allow your clients the connections they should be allowed. For example a domain with 15 DCs can need 15 to 30 minutes until a new or changed RR reaches the last DC.

In any case, please note the considerations regarding the time between the appearance of a fresh IP on authoritative nameservers and the moment the firewall gets aware of it. This time should be as short as possible, so that the firewall is aware of that IP hopefully before the first client tries to connect.



<!-- TOC --><a name="4-implementation"></a>
## 4 implementation


<!-- TOC --><a name="41-dnsmirror-collector"></a>
### 4.1 dnsmirror-collector

Running regularily, queries a list of known FQDNs from one or multiple upstream DNS servers, caches the replies and keeps them even after TTL expires, but only up to a (configurable) timeout after they are not recently observed anymore.


<!-- TOC --><a name="42-dnsmirror-zoneserver"></a>
### 4.2 dnsmirror-zoneserver

Running all the time, listening to tcp-udp/53, answering DNS queries from some backend, f. e. local files. Rebuilds these local files when triggered to do so by the dnsmirror- collector service. In case unknown FQDNs are queried, add these FQDNs to the list of known FQDNs, if they are resolvable at least once. (only possible if such information can be generated by the zoneserving DNS server and transported to the dnsmirror-collector). This may make practical usage easier, as new FQDNs can simply be added to the firewall ruleset and will automatically start working.

When building the new local zone files, order the entries by lastseen time, with the last-observed IP first, in case the firewall has an upper limit of IP addresses it can handle per FQDN. The DNS server should serve the IPs in a static list - all IPs in always the same order, no round robin - to support that.


<!-- TOC --><a name="43-data-transfer-from-collector-to-server"></a>
### 4.3 data transfer from collector to server

* generate zone files on disk and pass them via some mechanism (f. e. via copy operation on filesystem)
  * Always needs to generate a full zone file.
  * Needs
    * a mechanism to pull/push that zone file from sphere of influence of the dnsmirror to sphere of influence of the DNS server (separated user accounts, maybe separated systems)
    * a mechanism to notify of the availability of this new zone file
      * BIND9 would be accompanied by a service which pulls the new zone file, checks it for validity/sanity, places it into the zone directory of BIND9 and triggers (SIGHUP?) BIND9 to re-read its zone definitions.
* zone transfer (AXFR, IXFR) to a BIND9/Windows DC
  * This requires implementation of DNS server portion for zone transfer on dnsmirror/collector side, as a zone transfer is a pull operation:
    * A zone slave waits until a timeout or notification occurs, then requests the SOA from upstream server, compares the contained serial number of the zone, and if it's greater than the locally available zone copy requests an update via IXFR or AXFR.
* push dynamic DNS entry updates, to BIND9/Windows DC
  * nsupdate -g can do this via GSS-TSIG (basically Kerberos via DNS, transported in TKEY messages, until a dynamic signing key got exchanged which is then used to create a TSIG message)
    * needs working Kerberos (kinit?)
    * dnspython maybe can also do this


<!-- TOC --><a name="44-stages"></a>
### 4.4 stages

First, FQDNs are resolved, then the results are either cached, or directly pushed somewhere. In a full set of stages, you have the collector, the cache, and the backend.


<!-- TOC --><a name="441-collector"></a>
#### 4.4.1 collector

The collector is the stage, which queries the DNS servers for IPs.

* builtin (the one and only implementation)


<!-- TOC --><a name="442-cache"></a>
#### 4.4.2 cache

The cache adds the possibility to efficiently store the information per FQDN+IP, how long ago it was last observed, to allow a timeout/automatic deletion.

* ram (not implemented) (a volatile datastructure in ram)
* file (not implemented) (touch/delete a file per ip+fqdn in a configured directory)
* sqlite (not implemented)
* database [mariadb]


<!-- TOC --><a name="443-backend"></a>
#### 4.4.3 backend

The backend is the place, where the compiled list of IPs is stored, so that a firewall can retrieve it.

* rfc2136 [nsupdate-g]
* zonetransfer (not implemented)


<!-- TOC --><a name="444-tags-for-stage-implementations"></a>
#### 4.4.4 tags for stage implementations

* collector
* cache
  * accepts_stream (marks if a given implementation can/wants to receive data as it gets available, or if all data has to be assembled before handing it to the implementation in a single block)
* backend
  * accepts_stream (marks if a given implementation can/wants to receive data as it gets available, or if all data has to be assembled before handing it to the implementation in a single block)
  * readable_fqdn (if all known FQDNs can get read back)
  * readable_ip (if the configured IPs per FQDN can get read back)
  * readable_timestamp (additionally, if a timestamp per IP can be stored AND read back - allows to work without cache)
    * (store timestamps maybe in TXT RRs?)


<!-- TOC --><a name="5-open-to-do"></a>
## 5 open to-do

- [ ] warn if FQDNs are not resolved by a firewall for $amount-of-time - or just drop it from list of known FQDNs automatically.
  - [ ]  when using DCs as zone-serving devices this requires analyzing the debug log from the DC - maybe better to just analyze the firewall configs if all expected fqdn-objects exist
- [ ] more try-catch blocks
- [ ] warn if well-known FQDN has no active RR for both A+AAAA
- [ ] unit tests
- [ ] push RRs to all DCs instead of relying onto the domain-internal sync mechanism
- [ ] query authoritative DNS servers directly to learn new IPs as early as possible
  - [ ] they are not limited to always answer the same RRs until the TTL expired, as a proxy would be, and as such can be mirrored faster
- [ ] set short TTL to force the firewall to often check for new IPs (for example on ASA as low as 60s is possible, see "dns expire-entry-timer" - this is a hard-enforced minimum)
- [ ] stabilize calls to nsupdate - error handling of (un)expected cases
- [ ] implement removal of outdated RRs from AD-integrated dnsmirror-zone
  - [ ] build a database with the records and then synchronize from there instead directly pushing to the DCs
- [ ] the TTL of answers needs to be cached also, and IPs are kept in cache as long as the TTL is not expired, as clients may have done the same
- [ ] read the DC-integraded zone on import (first start with empty caching DB) and check, if it contains entries dnsmirror knows nothing about, then
  - [ ] only warn, or
  - [ ] warn, and import them with $maxlastseentime TTL, or
  - [ ] warn, and delete them (as it is unknown how old they are, possibly stale ; downside is degraded coverage of legitimate IPs)
- [ ] auto-renew ticket with credentials from .config
  - [ ] warn, info, when ticket will expire soon
- [ ] check access rights for .config file and refuse to run if other users can read it (as it potentially contains passwords)
- [ ] emit error if any configured FDQN is longer than 255-len(".$SUBDOMAIN.$DNSMIRRORZONE.")
- [ ] do something with the response statistic to mark servers as dead/alive and act on this mark
  - maybe save the mark somewhere and let it have a timeout
  - this requires to avoid queuing new threads with a task to query the dead server
  - this should only be done when ALL queries ended in a timeout, as there are reasons for a healthy server to not respond for specific queries
- [ ] test with IDN (internationalized domain names)


<!-- TOC --><a name="6-init"></a>
## 6 init


<!-- TOC --><a name="61-dbms-init"></a>
### 6.1 DBMS init

logging of SQL queries nice for debugging/audit:

    touch /var/log/mariadb_queries.log
    chgrp mysql /var/log/mariadb_queries.log
    chmod g+w /var/log/mariadb_queries.log


until DBMS restart:

    SET GLOBAL general_log_file='/var/log/mariadb_queries.log';
    SET GLOBAL general_log=1;


restart-/reboot-safe:

in the server config file, for example /etc/mysql/mariadb.conf.d/50-server.cnf

    [mariadb]
    general_log
    general_log_file=/var/log/mariadb_queries.log


<!-- TOC --><a name="62-database-init"></a>
### 6.2 database init

IPv6 addresses may be up to 61 chars long in their textual representation in absolute worst case. IPv4 embedded in IPv6 is 45, + zone identifier (separator %) up to 15 chars on Linux, unknown on other OS. While zone identifiers never should be part of a sane DNS response, we don't know what future brings.

FQDNs may be up to 254 chars (including root label) long (https://web.archive.org/web/20221116230829/https://devblogs.microsoft.com/oldnewthing/20120412-00/?p=7873), their encoding in absolutely worst case (\ddd) can be up to 1004 bytes long. While this encoding is unlikely and probably can be avoided, for now we are better safe than sorry.

Type is planned to be either "A" or "AAAA", however some more space is reserved.

See `db_init.sql` for commands to initialize the SQL tables.


<!-- TOC --><a name="63-python-init"></a>
### 6.3 python init

`make` will create the venv and download the requirements from requirements.txt
If you have issues getting the mariadb-connector installed, you may be missing the libmariadb-dev package (Debian), or your package is too old (Debian 11) and you need to pin an older version in the requirements.txt by specifying the version like so:

    mariadb==1.0.11


<!-- TOC --><a name="64-config-init"></a>
### 6.4 config init

Config goes into .config file in source/ directory, start with a copy of the .config.template file and add your database configuration etc. as needed.


<!-- TOC --><a name="65-windows-dns-zone-init"></a>
### 6.5 windows DNS zone init

If you want to use a Windows-hosted domain (was only tested on domain controllers, not stand-alone servers):

On any DC of the AD domain, set up a new AD-integrated primary zone, name f. e. dnsmirror.example.com (should be short as the length of this limits the possible length of mirrored FQDNs).

Disable scavenging, or, if it has to be enabled, set the timeouts to values which do not interfere with the needed maximum lifetime of the dynamic RRs.

Maybe remove rights for "Authenticated Users" to create objects and add the technical user as admin to the zone and/or the developer accounts/role.

For working access, you need a Kerberos token, see kinit, klist commands.

<!-- TOC --><a name="66-os-init"></a>
### 6.6 OS init

<!-- TOC --><a name="661-ulimit"></a>
#### 6.6.1 ulimit

Depending on the amount of domains, you may run into system limits. Set them e. g. to 65536 for soft and hard limit for open file handles (item "nofile") in e. g. /etc/security/limits.conf (exact location depends on your OS) and reboot (or figure out how to activate them live). When your user is "svc_dnsmirror", this could look like this:

    svc_dnsmirror	 soft	 nofile		 65536
    svc_dnsmirror	 hard	 nofile		 65536

<!-- TOC --><a name="662-kerberos"></a>
#### 6.6.2 Kerberos

Depending on the way you write your DNS updates to the DNS server you might need working Kerberos.

E. g. when using "nsupdate -g" to use GSS-TSIG to change a DNS zone on a Windows server: Your technical user needs to have access to a Kerberos 5 TGT (ticket-granting ticket), and for that you either need to manually log into your domain (using "kinit"), or you add your password to the .config file (might or might not be a good idea), or you generate a keytab file. How this exactly works, depends on your OS/distribution/installed packets/domain config, for me this worked:

    ktutil
    addent -password -p svc_dnsmirror@DOMAIN -k 1 -f
    # enter PASSWORD
    wkt ~/svc_dnsmirror.keytab
    quit

    # now you should have a keytab file in your home, let's use it - this should not ask you for a password
    kinit svc_dnsmirror@DOMAIN -k -t ~/svc_dnsmirror.keytab

    # now klist should show you at least one line containing "krbtgt":
    klist

Don't forget to add the path to the keytab file to the .config file of dnsmirror (and remove username/password, if you added it there), in the "dnsbackend.rfc2136" section into the "keytab" property.

TODO: check how to bind the keytab file to one host only, this seems rather complex, if possible at all

