PYTHON = python3
VENV = venv
PIP = $(VENV)/bin/pip

all: clean venv

clean:
	rm -rf $(VENV)
	rm -rf source/__pycache__/

venv:
	$(PYTHON) -m venv $(VENV)
	$(PIP) install -U pip
	$(PIP) install -r requirements.txt
