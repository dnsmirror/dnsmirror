# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import sys
import mariadb
import logging

logger = logging.getLogger("dnsmirror."+__name__)


class DNSStorage:
	# table: dnsmirror
	# COLUMNS: fqdn, ip, rrtype, lastseen
	#
	# fqdn:string:FQDN
	# ip:string:IPv4/IPv6
	# rrtype:string:"A","AAAA"
	# lastseen:int:unixtimestamp, the second that FQDN+IP combination was last received from any asked DNS server
	
	def __init__(self, sqlconfig):
		logger.debug("DNSStorage.__init__(self,config) ")# + str(sqlconfig))
		self.table = "dnscache"
		self.conn = mariadb.connect(host=sqlconfig["host"], port=sqlconfig["port"], user=sqlconfig["user"],
									password=sqlconfig["userpass"], database=sqlconfig["databasename"], autocommit=True)
		self.cur = self.conn.cursor(buffered=True)
	
	def get(self, fqdn, rrtype):
		# returns a list of IP addresses matching the given FQDN and rrtype (IPv4:"A", IPv6:"AAAA")
		logger.debug(f"DNSStorage.get(FQDN={fqdn},rrtype={rrtype})")
		self.cur.execute(f"SELECT ip FROM {self.table} WHERE `fqdn`=? AND `rrtype`=?;", (fqdn, rrtype))
		ip_list=[]
		for row in self.cur:
			ip_list.append(row[0])
		logger.debug(f"DNSStorage.get: for FQDN {fqdn} {rrtype} returning IPs {ip_list}")
		return ip_list
	
	def add_or_touch(self, fqdn, ip, rrtype, lastseen, threadid, taskid):
		"""function shall return the information, if the FQDN+IP+RRTYPE was added or updated, and in case of an update, the former lastseen value"""
		logger.debug(f"[TASK#{taskid}] DNSStorage.add_or_touch(fqdn={fqdn},rrtype={rrtype},ip={ip},lastseen={str(lastseen)})")
		logger.debug(f"[TASK#{taskid}] locking table {self.table}")
		self.cur.execute(f"LOCK TABLES {self.table} WRITE")
		logger.debug(f"[TASK#{taskid}] lock for table {self.table} received")
		try:
			self.cur.execute(f"SELECT fqdn,ip,rrtype,lastseen FROM {self.table} WHERE `fqdn`=? AND `ip`=? AND `rrtype`=?;", (fqdn, ip, rrtype))
		except mariadb.Error as e:
			logger.error(f"[TASK#{taskid}] Error: {e}")
			logger.debug(f"[TASK#{taskid}] unlocking tables")
			self.cur.execute("UNLOCK TABLES")
			sys.exit(40)
		if self.cur.rowcount == 0:
			logger.debug(f"[TASK#{taskid}] inserting new row")
			self.cur.execute(f"INSERT INTO {self.table} (fqdn,ip,rrtype,lastseen) VALUES (?,?,?,?);", (fqdn, ip, rrtype, lastseen))
			logger.debug(f"[TASK#{taskid}] unlocking tables")
			self.cur.execute("UNLOCK TABLES")
			return {'action':"inserted"}
		elif self.cur.rowcount == 1:
			oldvalues={}
			oldvalues['fqdn'],oldvalues['ip'],oldvalues['rrtype'],oldvalues['lastseen'] = self.cur.next()
			if (oldvalues['lastseen']==lastseen):
				logger.debug(f"[TASK#{taskid}] row already present")
				logger.debug(f"[TASK#{taskid}] unlocking tables")
				self.cur.execute("UNLOCK TABLES")
				return {'action': "unchanged"}
			else:
				self.cur.execute(f"UPDATE {self.table} SET lastseen=? WHERE fqdn=? AND ip=? AND rrtype=? LIMIT 1;", (lastseen, fqdn, ip, rrtype))
				logger.debug(f"[TASK#{taskid}] lastseen touched, old values: {oldvalues}")
				logger.debug(f"[TASK#{taskid}] unlocking tables")
				self.cur.execute("UNLOCK TABLES")
				return {'action':"touched", 'oldvalues':oldvalues}
		else:
			# more than one row means broken index, -1 may indicate error
			logger.error(f"[TASK#{taskid}] Error: add_or_update(), unexpected rowcount {self.cur.rowcount}")
			logger.debug(f"[TASK#{taskid}] unlocking tables")
			self.cur.execute("UNLOCK TABLES")
			sys.exit(41)
	
	
	def cleanup(self, lastseen):
		# deletes all rows which have a smaller lastseen value than the given one, returns the amount of lines which were removed
		logger.debug(f"DNSStorage.cleanup(lastseen={lastseen})")
		logger.debug(f"DNSStorage.cleanup: locking table {self.table}")
		self.cur.execute(f"LOCK TABLES {self.table} WRITE")
		logger.debug(f"DNSStorage.cleanup: lock for table {self.table} received")
		self.cur.execute(f"SELECT fqdn,ip,rrtype,lastseen FROM {self.table} WHERE lastseen < ?;", (lastseen,))
		deleted_amount = self.cur.rowcount
		if deleted_amount == 0:
			logger.debug("DNSStorage.cleanup: nothing to clean up")
			logger.debug("DNSStorage.cleanup: unlocking tables")
			self.cur.execute("UNLOCK TABLES")
			return 0
		deletedentries=self.cur.fetchall()
		logger.info(f"DNSStorage.cleanup: deleting {self.cur.rowcount} entries with lastseen smaller than {lastseen}")
		self.cur.execute(f"DELETE FROM {self.table} WHERE lastseen < ?;",(lastseen,))
		logger.debug("DNSStorage.cleanup: unlocking tables")
		self.cur.execute("UNLOCK TABLES")
		for entry in deletedentries:
			logger.info(f"DNSStorage.cleanup: deleted {entry}")
		return deleted_amount
