# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import dns.resolver
import logging
from threading import get_ident
import time

logger = logging.getLogger("dnsmirror."+__name__)


def dnsresolve(task: tuple) -> tuple:
    dnsserver, fqdn, rrtype, taskid = task
    threadid = get_ident()
    logger.debug(f"[TASK#{taskid}] task got picked up by THREAD#{threadid}")
    resolver = dns.resolver.Resolver(configure=False)
    resolver.nameservers = [dnsserver]
    error = ""
    starttime = time.time()
    try:
        response = resolver.resolve(fqdn, rrtype)
    except dns.resolver.NoAnswer:
        logger.debug(f"[TASK#{taskid}] NO ANSWER @{dnsserver} {fqdn} {rrtype}")
        response = []
        error = "noanswer"
    except (dns.resolver.LifetimeTimeout, dns.resolver.NoNameservers) as e:
        logger.debug(f"[TASK#{taskid}] TIMEOUT @{dnsserver} {fqdn} {rrtype}")
        response = []
        error = "timeout"
    endtime = time.time()
    duration = endtime - starttime
    if error == "" and duration > 2.0:
        error = "slow"
    lastseen = int(time.time())
    answers = []
    for answer in response:
        answers.append(
            {'dnsserver': dnsserver, 'fqdn': fqdn, 'ip': str(answer), 'rrtype': rrtype, 'lastseen': lastseen, 'duration': duration})
    logger.debug(f"[TASK#{taskid}] returning {len(answers)} answers, DNS RTT {round(duration,3)}s")
    return answers, error, threadid, task, duration
