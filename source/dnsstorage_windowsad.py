# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import logging
import subprocess

logger = logging.getLogger("dnsmirror."+__name__)


def call_nsupdate(cmds, threadid, taskid, max_attempts=2, attempt=1):
    STATE = ""
    logger.debug(f"[TASK#{taskid}] calling nsupdate: {cmds}")
    try:
        subprocess.run(["nsupdate", "-g"],
                       input="".join(cmds).encode(),
                       timeout=19,
                       capture_output=True,
                       check=True)
    except subprocess.TimeoutExpired as e:
        logger.error(f"TIMEOUT after {e.timeout}s in call_nsupdate({cmds}, {attempt})")
        if attempt < max_attempts:
            call_nsupdate(cmds, threadid, taskid, attempt + 1)
    except subprocess.CalledProcessError as e:
        error = e.stderr.decode()
        logger.error(f"ERROR in call_nsupdate({cmds}, {attempt}) = {e.returncode}: STDOUT: \"{e.stdout.decode()}\" ; STDERR: \"{e.stderr.decode()}\"")
        
        # decoding error
        if e.returncode == 1:
            if error.find("dns_request_getresponse: ") == 0:
                error = error[len("dns_request_getresponse: "):]
                if error.find("FORMERR") == 0:
                    STATE = "FORMERR"
                else:
                    STATE = "UNKNOWN_ERROR"
            elif error.find("could not reach any name server") == 0:
                STATE = "NO_NAMESERVER_REACHABLE"
            else:
                STATE = "UNKNOWN_ERROR"
        elif e.returncode == 2:
            if error.find("tkey query failed: ") == 0:
                error = error[len("tkey query failed: "):]
                if error.find("GSSAPI error: ") == 0:
                    error = error[len("GSSAPI error: "):]
                    if error.find("Major = Unspecified GSS failure.  Minor code may provide more information, ") == 0:
                        error = error[len("Major = Unspecified GSS failure.  Minor code may provide more information, "):]
                        if error.find("Minor = No Kerberos credentials available") == 0:
                            STATE = "KERBEROS_NO_TICKET"
                        elif error.find("Minor = Ticket expired.") == 0:
                            STATE = "KERBEROS_EXPIRED_TICKET"
                        else:
                            STATE = "UNKNOWN_ERROR"
                    else:
                        STATE = "UNKNOWN_ERROR"
                else:
                    STATE = "UNKNOWN_ERROR"
            elif error.find("; TSIG error with server: ") == 0:
                error = error[len("; TSIG error with server: "):]
                if error.find("tsig verify failure") == 0:
                    STATE = "TSIG_VERIFY_FAILURE"
                else:
                    STATE = "UNKNOWN_ERROR"
            elif error.find("; Communication with ") == 0:
                error = error[len("; Communication with "):]
                error = error[error.find(" failed: ")+len(" failed: "):]
                if error.find("operation canceled") == 0:
                    STATE = "COMMUNICATION_CANCELED"
                else:
                    STATE = "UNKNOWN_ERROR"
            else:
                STATE = "UNKNOWN_ERROR"
        else:  # unexpected returncode
            STATE = "UNKNOWN_ERROR"
        logger.error(STATE)
        
        if STATE == "KERBEROS_NO_TICKET":
            # TODO: call kinit
            raise
        elif STATE == "KERBEROS_EXPIRED_TICKET":
            # TODO: call kinit
            raise
        elif STATE == "FORMERR":
            # TODO: Analyze how this error can happen at all - for now, we just hope the best and keep going. The NS update did probably NOT happen in this case.
            logger.exception(e)
            return
        elif STATE == "NO_NAMESERVER_REACHABLE":
            # don't know what to do here except retrying - maybe over and over?
            pass
        elif STATE == "COMMUNICATION_CANCELED":
            # retry.
            pass
        elif STATE == "TSIG_VERIFY_FAILURE":
            # this happens either seemingly randomly, or reproducibly when the destination FQDN has over thousand-something entries
            pass
        elif STATE == "UNKNOWN_ERROR":
            # unexpected returncode or unexpected error string
            raise
        else:  # unexpected STATE
            logger.error(f"in unexpected STATE {STATE}")
            raise
        
        if attempt < max_attempts:
            logger.debug(f"retrying call_nsupdate({cmds})")
            call_nsupdate(cmds, threadid, taskid, max_attempts, attempt + 1)
        else:
            logger.warning(f"attempts exceeded, giving up on call_nsupdate({cmds})")
            return


class DNSStorage:
    def __init__(self, config):
        self.dnsmirrorzone = config['dnsmirrorzone']
        self.subdomain     = config['subdomain']
        if len(self.subdomain) == 0:
            self.parentdomain = self.dnsmirrorzone
        else:
            self.parentdomain = f"{self.subdomain}.{self.dnsmirrorzone}"
            logger.info(f"inserting all RRs into configured subdomain ({self.parentdomain})")
    
    def add_or_touch(self, fqdn, ip, rrtype, lastseen, threadid, taskid):
        ttl = 60
        
        cmds = [f'zone {self.dnsmirrorzone}\n',
                f'update add {fqdn}.{self.parentdomain} {ttl} {rrtype} {ip} \n',
                f'send\n']
        call_nsupdate(cmds, threadid, taskid)
        return {'action': "touched", 'oldvalues': {'fqdn': fqdn, 'ip': ip, 'rrtype': rrtype, 'lastseen': 0}}
    
    def cleanup(self, lastseen):
        # STUB
        return 0
    
    def get(self, fqdn, rrtype):
        # STUB
        return []

    def get_all_DCs(self):
        """
        finds out about all the DCs in the SOA for the given zone
        """
        # read SOA from apex -> get_list_of_DCs()
        # DC_healthcheck() - info if unhealthy
        # read zone/subdomain via DNS
        # get list of existent domains + RRs, per DC
        # compare to cache, diff
        # -> add_or_touch()/delete()
