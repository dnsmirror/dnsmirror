# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import concurrent.futures
import dnsresolver
import logging
import sys

logger = logging.getLogger("dnsmirror."+__name__)


def update_dnscache(dnsstorage, config):
    logger.debug("update_dnscache()")
    dnsservers, fqdns, rrtypes = config['dnsservers'], config['fqdns'], config['rrtypes']
    parallelqueries = config['builtin']['parallelqueries']
    parallelupdates = config['builtin']['parallelupdates']
    dnsserverstatistics = {}
    
    logger.info(f"querying {len(dnsservers)} dnsserver(s): {dnsservers}")
    logger.info(f"querying for {len(fqdns)} fqdn(s): {fqdns}")
    logger.info(f"querying for {len(rrtypes)} rrtype(s): {rrtypes}")
    
    for dnsserver in dnsservers:
        dnsserverstatistics[dnsserver] = {'queries': 0, 'ok': 0, 'timeout': 0, 'noanswer': 0, 'slow': 0}
    
    logger.info(f'starting up to {parallelqueries} workers to query dns servers')
    questionexecutor = concurrent.futures.ThreadPoolExecutor(max_workers=parallelqueries)
    questiontasks = []
    
    taskid = 0
    # TODO: fetch fqdn and rrtype from database, only query combinations which are listed there
    for fqdn in fqdns:
        for rrtype in rrtypes:
            for dnsserver in dnsservers:
                task = (dnsserver, fqdn, rrtype, str(taskid))
                logger.debug(f"[TASK#{taskid}] scheduled query: @{dnsserver} {fqdn} {rrtype}")
                dnsserverstatistics[dnsserver]['queries'] += 1
                questiontasks.append(questionexecutor.submit(dnsresolver.dnsresolve, task))
                taskid += 1
    logger.info(f'scheduled {len(questiontasks)} tasks to query dns servers, using up to {parallelqueries} workers')
    questionexecutor.shutdown(wait=False, cancel_futures=False)
    
    actionstatistics = {'inserted': 0, 'touched': 0, 'unchanged': 0}
    result = {'statechanged': False}  # tracks if the zonefile needs to get updated
    
    updateexecutor = concurrent.futures.ThreadPoolExecutor(max_workers=parallelupdates)
    updatetasks = []
    
    logger.info(f'starting up to {parallelupdates} workers to push IPs')
    for questiontask in concurrent.futures.as_completed(questiontasks):
        questiontasks.remove(questiontask)
        partialanswers, error, threadid, task, duration = questiontask.result()
        dnsserver, fqdn, rrtype, taskid = task
        logger.debug(f"[TASK#{taskid}] analyzing result of questiontask, {len(questiontasks)} questiontasks remaining in list")

        if error == "":
            dnsserverstatistics[dnsserver]['ok'] += 1
        elif error == "timeout":
            logger.info(f"[TASK#{taskid}] got TIMEOUT after {round(duration,3)}s while querying server @{dnsserver} {fqdn} {rrtype}")
            dnsserverstatistics[dnsserver]['timeout'] += 1
        elif error == "noanswer":
            logger.debug(f"[TASK#{taskid}] got NOANSWER after {round(duration,3)}s while querying server @{dnsserver} {fqdn} {rrtype}")
            dnsserverstatistics[dnsserver]['noanswer'] += 1
        elif error == "slow":
            dnsserverstatistics[dnsserver]['slow'] += 1
            logger.debug(f"[TASK#{taskid}] SLOW answer after {round(duration,3)}s while querying server @{dnsserver} {fqdn} {rrtype}")
        else:
            logger.error(f"[TASK#{taskid}] got unexpected error {error} after {round(duration,3)}s while querying @{dnsserver} {fqdn} {rrtype}")
        if len(partialanswers) > 0:
            answerid = 0
            for answer in partialanswers:
                updatetasks.append(updateexecutor.submit(ingest_answer, *[dnsstorage, answer, threadid, taskid + "." + str(answerid), {'inserted': 0, 'touched': 0, 'unchanged': 0}]))
                answerid += 1
    updateexecutor.shutdown(wait=False, cancel_futures=False)
    logger.info("finished all tasks to query dns servers")
    logger.info(f'scheduled {len(updatetasks)} tasks to push IPs, using up to {parallelupdates} workers')

    for updatetask in concurrent.futures.as_completed(updatetasks):
        updatetasks.remove(updatetask)
        updateresult = updatetask.result()
        logger.debug(f"[TASK#{updateresult['taskid']}] analyzing result of updatetask, {len(updatetasks)} updatetasks remaining in list")
        
        if updateresult['statechanged']:
            result['statechanged'] = True
        actionstatistics['inserted'] += updateresult['actionstatistics']['inserted']
        actionstatistics['touched'] += updateresult['actionstatistics']['touched']
        actionstatistics['unchanged'] += updateresult['actionstatistics']['unchanged']
    logger.info("finished all tasks to push IPs")
    
    logger.debug(f"response statistics for dnsservers: {dnsserverstatistics}")
    deadservers = 0
    for server in dnsserverstatistics:
        if dnsserverstatistics[server]['timeout'] > 0 or dnsserverstatistics[server]['slow'] > 0:
            logger.info(f"response statistics suspicious for server {server} (slow>0|timeout>0): {dnsserverstatistics[server]}")
        if dnsserverstatistics[server]['ok'] == 0 and dnsserverstatistics[server]['queries'] > 0:
            logger.warning(f"dnsserver {server} seems dead (zero acceptable responses): {dnsserverstatistics[server]}")
            deadservers += 1
    if deadservers == len(dnsserverstatistics):
        logger.critical(f"ALL dnsservers seem dead (zero acceptable responses)")
    result['actionstatistics'] = actionstatistics
    return result


def ingest_answer(dnsstorage, answer, threadid, taskid, actionstatistics):
    statechanged = False
    ip, rrtype, lastseen, dnsserver, fqdn = answer['ip'], answer['rrtype'], answer['lastseen'], answer['dnsserver'], answer['fqdn']
    logger.debug(f"[TASK#{taskid}] @{dnsserver} replied {fqdn} -> {rrtype} {ip}, {lastseen}")
    result = dnsstorage.add_or_touch(fqdn, ip, rrtype, lastseen, threadid, taskid)
    if result['action'] == "inserted":
        actionstatistics['inserted'] += 1
        logger.info(f"[TASK#{taskid}] inserted: server {dnsserver} replied new RR: {fqdn} -> {rrtype} {ip}, lastseen {lastseen}")
        statechanged = True
    elif result['action'] == "touched":
        actionstatistics['touched'] += 1
        logger.debug(
            f"[TASK#{taskid}] touched after {lastseen - result['oldvalues']['lastseen']} seconds, former lastseen: {result['oldvalues']['lastseen']}")
        pass
    elif result['action'] == "unchanged":
        actionstatistics['unchanged'] += 1
        logger.debug(f"[TASK#{taskid}] unchanged")
        pass
    else:
        logger.error(f"[TASK#{taskid}] unexpected action returned: {result['action']}")
        sys.exit(30)
    return {'statechanged': statechanged, 'actionstatistics': actionstatistics, 'taskid': taskid}


def add_new_fqdn(dnsstorage, fqdn, force=True, dnsservers=None):
    """For force=True we just add the fqdn to permanent config and hope the caller knows what he does.
       For force=False: In case we automatically want to add all FQDNs this service is asked about,
       we check all upstream DNS servers until at least one knows an answer - if so, we add the FQDN to our list.
       In this case, this function is expected to get called very often and should implement a cooldown time for
       FQDNs to avoid uselessly overloading the upstream DNS servers.
       """
    if force is False:
        raise Exception("in add_fqdn(): auto-mode using force=False not implemented")
        # check if fqdn is already in permanent config
        # check if fqdn is in fqdn cooldown list and add if not
        # This check should be implemented as locally as possible, without involving complex mechanisms like a DBMS lookup.
        # It could be a local directory with empty files named after the domain (maybe hashed to ensure the filesystem
        # can accept all chars used in domains), which are stat'ed and touch'ed at each hit and cleaned up once every
        # few minutes/hours. Either as scheduled task, or with a low probability on each call of this function.
        # for dnsserver in dnsservers
        #     for rrtype in ("A", "AAAA"):
        #         responses = dnslookup(fqdn, rrtype)  # must be parallel due to expected high call rate
        #         for response in responses
        #             # analyze responses - if zero successful lookups, return without pushing fqdn (fqdn seems bogus)
    
    # per default push both rrtypes, but keep them separated entries to make it possible to remove one from the database
    push_fqdn_to_config(dnsstorage, fqdn, "A")
    push_fqdn_to_config(dnsstorage, fqdn, "AAAA")


def push_fqdn_to_config(dnsstorage, fqdn, rrtype):
    logger.debug(f"push_fqdn_to_config({dnsstorage},{fqdn},{rrtype})")
    logger.error(f"push_fqdn_to_config() not implemented, did NOT add {fqdn} {rrtype} to permanent config")
