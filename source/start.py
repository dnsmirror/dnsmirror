# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import getopt
import logging
import main
import sys
import time
import kerberos

starttime = time.time()

mainlogger = logging.getLogger("dnsmirror")
mainlogger.setLevel(logging.WARN)
mainlogger_handler = logging.StreamHandler()
mainlogger_formatter = logging.Formatter('%(asctime)s.%(msecs)03d  %(name)-35s %(funcName)-25s %(levelname)-8s %(message)s', '%Y-%m-%d  %H:%M:%S')
mainlogger_handler.setFormatter(mainlogger_formatter)
mainlogger.addHandler(mainlogger_handler)

logger = logging.getLogger("dnsmirror.start")

earlyerrorlogger = logging.getLogger("dnsmirror.start.earlyerror")
earlyerrorlogger.propagate = False
earlyerrorlogger_handler = logging.StreamHandler()
earlyerrorlogger.addHandler(earlyerrorlogger_handler)


def seconds_till_next_run(starttime_, runevery_):
    """returns the seconds until the next multiple of runevery since starttime begins
       """
    return runevery_ - ((time.time() - starttime_) % runevery_)


repeatmain = False
runevery = 0  # unneeded assignment to silence the static code analyzer
secondsformatstring = "{0:.3f}"
configfile = ".config"


def print_parameter_help():
    earlyerrorlogger.error("[--help|-h|-?] This help.")
    earlyerrorlogger.error("[--debug=(debug|info|warn[ing]|error|critical)] (DEFAULT=warning)")
    earlyerrorlogger.error("[--loop=SECONDS] (int) If set, the program will run in a loop with SECONDS between the start of the loop iterations, if possible.")
    earlyerrorlogger.error("[--config=FILE] (DEFAULT=.config)")


try:
    opts, args = getopt.gnu_getopt(sys.argv, "h?", ["debug=", "DEBUG=", "loop=", "config=", "help"])
except getopt.GetoptError:
    earlyerrorlogger.error("wrong parameter set")
    earlyerrorlogger.error("valid:")
    print_parameter_help()
    sys.exit(10)


# TODO: check for unexpected superfluous parameters without leading dashes (neither short- nor longoptions)

for o, a in opts:
    if o.lower() == "--help" or o.lower() == "-h" or o.lower() == "-?":
        print_parameter_help()
        sys.exit(0)
    elif o.lower() == "--debug":
        if a.lower() == "debug":
            globalloglevel = logging.DEBUG
        elif a.lower() == "info":
            globalloglevel = logging.INFO
        elif a.lower() == "warn" or a.lower() == "warning":
            globalloglevel = logging.WARN
        elif a.lower() == "error":
            globalloglevel = logging.ERROR
        elif a.lower() == "critical":
            globalloglevel = logging.CRITICAL
        else:
            earlyerrorlogger.error(f"unknown --debug parameter \"{a}\"")
            sys.exit(11)
        mainlogger.setLevel(globalloglevel)
    elif o.lower() == "--loop":
        repeatmain = True
        runevery = int(a)  # TODO: check if it could be a float to allow extra-fast loops
        if runevery <= 0:
            earlyerrorlogger.error("loop duration must be positive")
            sys.exit(11)
    elif o.lower() == "--config":
        configfile = a
    else:
        earlyerrorlogger.error(f"unhandled parameter \"{o}\" \"{a}\"")
        sys.exit(11)

logger.debug("early setup phase finished, starting to loop")


while True:
    loopstarttime = time.time()
    kerberos.renew_TGT()
    logger.debug("LOOP main()")
    main.main({'configfile': configfile})
    loopduration = time.time() - loopstarttime
    logger.info(f"LOOP main() finished after {secondsformatstring.format(loopduration)}s")
    if repeatmain:
        if loopduration > runevery:
            logger.warning(f"loop run took {secondsformatstring.format(loopduration)}s, {secondsformatstring.format(loopduration-runevery)}s longer than the time after which the loop is supposed to run again ({secondsformatstring.format(runevery)}s) -> not sleeping")
        else:
            secondstosleep = seconds_till_next_run(starttime, runevery)
            logger.info(f"sleeping for {secondsformatstring.format(secondstosleep)}s until next run")
            time.sleep(secondstosleep)
    else:
        break


endtime = time.time()
logger.info(f"finished after {secondsformatstring.format(endtime - starttime)}s")
