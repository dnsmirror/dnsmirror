# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import logging
import subprocess

logger = logging.getLogger("dnsmirror."+__name__)


def get_TGT(self):
    """get a fresh Kerberos TGT"""


def renew_TGT():
    """renew an existing Kerberos TGT within it's lifetime"""
    logger.info("renewing kerberos ticket")
    logger.debug("run(\"kinit -R\")")
    kinit_result = subprocess.run(["kinit", "-R"], capture_output=True)
    logger.debug(f"run(\"kinit -R\")={kinit_result.returncode} returned, stdout:\"{kinit_result.stdout.decode()}\", stderr:\"{kinit_result.stderr.decode()}\"")
