#!/bin/bash

CONFIGFILE=.config

HOST="$(sed -n '/\[dnsbackend.database\]/,$p' $CONFIGFILE|grep '^\s*host\s*='        |head -n1|cut -d= -f2|cut -d# -f1|xargs)"
PORT="$(sed -n '/\[dnsbackend.database\]/,$p' $CONFIGFILE|grep '^\s*port\s*='        |head -n1|cut -d= -f2|cut -d# -f1|xargs)"
USER="$(sed -n '/\[dnsbackend.database\]/,$p' $CONFIGFILE|grep '^\s*user\s*='        |head -n1|cut -d= -f2|cut -d# -f1|xargs)"
PASS="$(sed -n '/\[dnsbackend.database\]/,$p' $CONFIGFILE|grep '^\s*userpass\s*='    |head -n1|cut -d= -f2|cut -d# -f1|xargs)"
DB="$(  sed -n '/\[dnsbackend.database\]/,$p' $CONFIGFILE|grep '^\s*databasename\s*='|head -n1|cut -d= -f2|cut -d# -f1|xargs)"

mariadb --defaults-file=<(echo -e "[client]\npassword=${PASS}\n") -h$HOST -P$PORT -u$USER $DB
