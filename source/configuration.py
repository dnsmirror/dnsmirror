# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) Carsten Weber 2022

import configparser
import logging

logger = logging.getLogger("dnsmirror."+__name__)


def import_config(configfile):
    # ConfigParser does not allow to store lists/dicts in the resulting data structure (just str, int, float, bool).
    # It is primarily used to avoid programming the whole config ingestion thing with all kinds of exceptions.
    # Its shortcomings seem to make it necessary to transform the datastructure into a dict.
    rawconfig = configparser.ConfigParser(strict=True, inline_comment_prefixes=";#")
    configfiles = [configfile]
    if rawconfig.read(configfiles) == []:
        raise Exception(f'no config file found - searched: {configfiles}')
    
    config = {}

    # dnscollector
    config['dnscollector'] = {}
    config['dnscollector']['type'] = str(rawconfig['dnscollector']['type'])
    if config['dnscollector']['type'] == "builtin":
        config['dnscollector']['builtin'] = {}
        config['dnscollector']['builtin']['parallelqueries'] = max(1, int(rawconfig['dnscollector.builtin']['parallelqueries']))
    else:
        raise Exception(f"dnscollector type {config['dnscollector']['type']} not implemented")
    # lists get deduplicated on import:
    config['dnscollector']['dnsservers-source'] = str(rawconfig['dnscollector']['dnsservers-source'])
    if config['dnscollector']['dnsservers-source'] == "configfile":
        config['dnscollector']['dnsservers'] = list(dict.fromkeys(str(rawconfig['dnscollector']['dnsservers']).split()))
    else:
        raise Exception(f"dnscollector dnsservers-source {config['dnscollector']['dnsservers-source']} not implemented")
    config['dnscollector']['fqdns-source'] = str(rawconfig['dnscollector']['fqdns-source'])
    if config['dnscollector']['fqdns-source'] == "configfile":
        config['dnscollector']['fqdns'] = list(dict.fromkeys(str(rawconfig['dnscollector']['fqdns']).split()))
    else:
        raise Exception(f"dnscollector fqdns-source {config['dnscollector']['fqdns-source']} not implemented")
    config['dnscollector']['rrtypes-source'] = str(rawconfig['dnscollector']['rrtypes-source'])
    if config['dnscollector']['rrtypes-source'] == "configfile":
        config['dnscollector']['rrtypes'] = list(dict.fromkeys(str(rawconfig['dnscollector']['rrtypes']).split()))
    else:
        raise Exception(f"dnscollector rrtypes-source {config['dnscollector']['rrtypes-source']} not implemented")

    # dnscache
    config['dnscache'] = {}
    config['dnscache']['type'] = str(rawconfig['dnscache']['type'])
    if config['dnscache']['type'] == 'database':
        config['dnscache']['database'] = {}
        config['dnscache']['database']['connector'] = str(rawconfig['dnscache.database']['connector'])
        if config['dnscache']['database']['connector'] == "mariadb":
            config['dnscache']['database'] = {'host': str(rawconfig['dnscache.database']['host']),
                                              'port': int(rawconfig['dnscache.database']['port']),
                                              'databasename': str(rawconfig['dnscache.database']['databasename']),
                                              'user': str(rawconfig['dnscache.database']['user']),
                                              'userpass': str(rawconfig['dnscache.database']['userpass']),
                                              'admin': str(rawconfig['dnscache.database']['admin']),
                                              'adminpass': str(rawconfig['dnscache.database']['adminpass']), }
        else:
            raise Exception(f"dnscache.database connector {config['dnscache']['database']['connector']} not implemented")
    elif config['dnscache']['type'] == 'none':
        pass
    else:
        raise Exception(f"dnscache type {config['dnscache']['type']} not implemented")
    
    # dnsbackend
    config['dnsbackend'] = {}
    config['dnsbackend']['maxlastseentime'] = int(rawconfig['dnsbackend']['maxlastseentime'])
    config['dnsbackend']['type'] = str(rawconfig['dnsbackend']['type'])
    if config['dnsbackend']['type'] == "rfc2136":
        config['dnsbackend']['rfc2136'] = {}
        config['dnsbackend']['rfc2136']['connector'] = str(rawconfig['dnsbackend.rfc2136']['connector'])
        if config['dnsbackend']['rfc2136']['connector'] == "nsupdate-g":
            pass
        else:
            raise Exception(f"dnsbackend.rfc2136 connector {config['dnsbackend']['rfc2136']['connector']} not implemented")
        config['dnsbackend']['rfc2136']['dnsmirrorzone'] = str(rawconfig['dnsbackend.rfc2136']['dnsmirrorzone'])
        config['dnsbackend']['rfc2136']['subdomain'] = str(rawconfig['dnsbackend.rfc2136']['subdomain'])
        config['dnsbackend']['rfc2136']['parallelupdates'] = max(1, int(rawconfig['dnsbackend.rfc2136']['parallelupdates']))
        config['dnsbackend']['rfc2136']['krbuser'] = str(rawconfig['dnsbackend.rfc2136']['krbuser'])
        config['dnsbackend']['rfc2136']['krbpass'] = str(rawconfig['dnsbackend.rfc2136']['krbpass'])
        if config['dnsbackend']['rfc2136']['krbpass']:
            logger.warning("cleartext password in config (krbpass field not empty) - this is discouraged, use a kerberos keytab file instead")
        config['dnsbackend']['rfc2136']['keytab'] = str(rawconfig['dnsbackend.rfc2136']['keytab'])
    elif config['dnsbackend']['type'] == "none":
        pass
    else:
        raise Exception(f"dnsbackend type {config['dnsbackend']['type']} not implemented")
    
    return config
