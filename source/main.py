# SPDX-License-Identifier: GPL-3.0-or-later
#
#    This file is part of dnsmirror.
#
#    dnsmirror is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    dnsmirror is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Carsten Weber 2022

import logging
import time
import sys

import collector
import configuration

logger = logging.getLogger("dnsmirror."+__name__)


def main(mainparams):
    configfile = mainparams['configfile']
    
    config = configuration.import_config(configfile)
    config['dnscollector']['builtin']['parallelupdates'] = config['dnsbackend']['rfc2136']['parallelupdates']  # "workaround", functions need to get restructured
    
    # TODO: fix selection logic, broken since major .config format overhaul: only direct write via rfc2136 works
    if config['dnsbackend']['type'] == "rfc2136":
        import dnsstorage_windowsad
        dnsstorage = dnsstorage_windowsad.DNSStorage(config['dnsbackend']['rfc2136'])
    elif config['dnsbackend']['type'] == "database":
        import dnsstorage_mariadb
        dnsstorage = dnsstorage_mariadb.DNSStorage(config['dnsbackend']['database'])
    else:
        logger.error(f"unknown dnsbackend {config['dnsbackend']['type']}")
        sys.exit(20)
    
    collectorstate = collector.update_dnscache(dnsstorage, config['dnscollector'])
    statechanged = collectorstate['statechanged']
    actionstatistics = collectorstate['actionstatistics']
    
    # remove outdated entries from cache
    amount_deleted = dnsstorage.cleanup(int(time.time()) - config['dnsbackend']['maxlastseentime'])
    if amount_deleted > 0:
        statechanged = True
    actionstatistics['deleted'] = amount_deleted
    
    if statechanged:
        logger.info("need to rebuild zonefile")
        # trigger_dnscache_rebuild() // via zonetransfer mechanism? dnspython may support that

    logger.info(f"statistics: inserted: {actionstatistics['inserted']}, touched: {actionstatistics['touched']}, deleted: {actionstatistics['deleted']}")

